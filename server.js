const express = require('express');
const app = express();
const port = 3000;

const bodyparser = require('body-parser');
const sequelize = require('./config/sequelize');

sequelize.initDb();

app.use(bodyparser.json());



app.listen(port, ()=>console.log('Votre appliaction démarre sur le port' +port));