module.exports = (sequelize, Datatypes) => {
    return sequelize .define('Order',{
        id: {
            type : Datatypes.INTEGER,
            primaryKey : true,
            autoIncrement : true,
        },
        orderName: {
            type : Datatypes.STRING,
            allowNull : false,
        },
        orderNumber : {
            type : Datatypes.INTEGER,
            allowNull : false,
        },
        orderDate : {
            type : Datatypes.INTEGER,
            allowNull : false,  
        },
        orderQuantity : {
            type : Datatypes.INTEGER,
            allowNull : false,
        },     
        orderTotalAmount: {
            type : Datatypes.INTEGER,
            allowNull : false
        },
        orderCustomer : {
            type : Datatypes.INTEGER,
            allowNull: false
        },
        orderDeliveryAddress: {
            type : Datatypes.STRING,
            allowNull : false,
        },
        orderDiscountAmount : {
            type : Datatypes.INTEGER,
            allowNull : false
        },
        orderDeliveryFee : {
            type : Datatypes.INTEGER,
            allowNull : false
        }
    });
}