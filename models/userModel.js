module.exports = (sequelize, Datatypes) => {
    return sequelize .define('User',{
        userId: {
            type : Datatypes.INTEGER,
            primaryKey : true,
            autoIncrement : true,
        },
        userName: {
            type : Datatypes.STRING,
            unique : { msg: "Ce nom est déjà pris" },
            allowNull : false,
        },
        userAddress : {
            type : Datatypes.STRING,
            allowNull : false,
        },
        userCity : {
            type : Datatypes.STRING,
            allowNull : false,  
        },
        userSexe : {
            type : Datatypes.STRING,
            allowNull : false,
        },     
        userEmail: {
            type : Datatypes.STRING,
            allowNull : false
        },
        userPhone : {
            type : Datatypes.INTEGER,
            allowNull: false
        },
        userPassword: {
            type : Datatypes.STRING,
            allowNull : false,
        },
    });
}